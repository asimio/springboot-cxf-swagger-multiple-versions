package com.asimio.cxfdemo.multiversion.rest.v1.model;

public class Hello {

	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}