package com.asimio.cxfdemo.multiversion.rest.config;

import org.apache.cxf.feature.Feature;
import org.apache.cxf.jaxrs.swagger.Swagger2Feature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeaturesConfig {

	@Value("${cxf.path}")
	private String basePath;

	@Bean("swagger2Feature")
	public Feature swagger2FeatureV1() {
		return new Swagger2Feature();
	}
}