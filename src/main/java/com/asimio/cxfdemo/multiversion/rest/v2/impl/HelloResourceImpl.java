package com.asimio.cxfdemo.multiversion.rest.v2.impl;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.asimio.cxfdemo.multiversion.rest.v2.HelloResource;
import com.asimio.cxfdemo.multiversion.rest.v2.model.Hello;

// No JAX-RS annotation in class, method or method arguments
@Component("helloResourceV2")
public class HelloResourceImpl implements HelloResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(HelloResourceImpl.class);

	@Override
	public Response getHelloVersion2InUrl(String name) {
		LOGGER.info("getHelloVersionInUrl() v2");
		return this.getHello(name, "Version 2 - passed in URL");
	}

	@Override
	public Response getHelloVersion2InAcceptHeader(String name) {
		LOGGER.info("getHelloVersionInAcceptHeader() v2");
		return this.getHello(name, "Version 2 - passed in Accept Header");
	}

	private Response getHello(String name, String partialMsg) {
		if ("404".equals(name)) {
			return Response.status(Status.NOT_FOUND).build();
		}
		Hello result = new Hello();
		result.setMsg1(String.format("Hello %s. %s", name, partialMsg));
		result.setMsg2("From Orlando");
		return Response.status(Status.OK).entity(result).build();
	}

}